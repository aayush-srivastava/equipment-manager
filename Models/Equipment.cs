﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EquipmentAssignment.Models
{
    public class Equipment
    {
        [Display(Name = "Equipment Number")]
        public int Id { get; set; }
        [Display(Name = "Destination Address")]
        public string Address { get; set; }
        [Display(Name = "Contract Start Date")]
        [DataType(DataType.Date)]
        public DateTime startDate { get; set; }
        [Display(Name = "Contract End Date")]
        [DataType(DataType.Date)]
        public DateTime endDate { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}