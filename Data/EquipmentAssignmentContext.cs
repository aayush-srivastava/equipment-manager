﻿using Microsoft.EntityFrameworkCore;
using EquipmentAssignment.Models;

namespace EquipmentAssignment.Data
{
    public class EquipmentAssignmentContext : DbContext
    {
        public EquipmentAssignmentContext(DbContextOptions<EquipmentAssignmentContext> options)
            : base(options)
        {
        }

        public DbSet<Equipment> Equipment { get; set; }
    }
}